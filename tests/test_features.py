"""Test features."""
import numpy as np  # type: ignore

from rl_function_approximation import features


def test_linear_ϕ() -> None:
    """test_linear_ϕ."""
    S = np.array([1, 2])
    expected = np.array([1, 2, 1])
    res = features.linear_ϕ(S)
    np.testing.assert_array_equal(res, expected)


def test_ϕ_drop() -> None:
    """test_ϕ_drop."""
    S = np.array([1, 2])
    expected = np.array([2])
    ϕ = features.ϕ_drop(np.array([1]), lambda x: x)
    res = ϕ(S)
    np.testing.assert_array_equal(res, expected)


def test_one_hot_ϕ() -> None:
    """test_one_hot_ϕ."""
    S = 1
    expected = np.array([0, 1, 1])
    ϕ = features.one_hot_ϕ(2)
    res = ϕ(S)
    np.testing.assert_array_equal(res, expected)
